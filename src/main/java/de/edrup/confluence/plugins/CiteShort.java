package de.edrup.confluence.plugins;

import com.atlassian.confluence.xhtml.api.XhtmlContent;

public class CiteShort extends Cite {

	public CiteShort(XhtmlContent xhtmlUtils, CiteManager citeMan) {
		super(xhtmlUtils, citeMan);
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}
}
