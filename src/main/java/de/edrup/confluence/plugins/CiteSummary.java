package de.edrup.confluence.plugins;

import java.util.ArrayList;
import java.util.Map;

import javax.xml.stream.XMLStreamException;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.FormatConverter;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.sal.api.message.I18nResolver;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CiteSummary implements Macro {

	private final XhtmlContent xhtmlUtils;
	private final CiteManager citeMan;
	private final FormatConverter formatConverter;
	private final I18nResolver i18n;
	
	private static final Logger log = LoggerFactory.getLogger(CiteSummary.class);

	public CiteSummary(XhtmlContent xhtmlUtils, CiteManager citeMan, FormatConverter formatConverter,
		I18nResolver i18n) {
		this.xhtmlUtils = xhtmlUtils;
		this.citeMan = citeMan;
		this.formatConverter = formatConverter;
		this.i18n = i18n;
	}

	@Override
	public String execute(Map<String, String> parameters, String bodyContent,
			final ConversionContext conversionContext) throws MacroExecutionException {
		
		// get the parameters we need
		String bibTEXFormat = parameters.containsKey("format") ? parameters.get("format") : "ieee";
		String local = parameters.containsKey("local") ? parameters.get("local") : "false";
		
		// prepare the summary
		String body = conversionContext.getEntity().getBodyAsString();
		if(body.contains("data-macro-name")) {
			try {
				body = formatConverter.convertToStorageFormat(body, conversionContext.getEntity().toPageContext());
			}
			catch(Exception e) {
				log.error(e.toString());
				return i18n.getText("de.edrup.confluence.plugins.simple-cite.message.preview.error");
			}
		}
		citeMan.prepareForSummary(body, bibTEXFormat, conversionContext, conversionContext.getEntity());
		
		// get cites and cite counts
		ArrayList<String> cites = citeMan.getCites();
		ArrayList<String> citeScopes = citeMan.getCiteScopes();
		
		// generate visual output
		StringBuilder outs = new StringBuilder();
		outs.append("<ol>");
			
		for(int n = 0; n < cites.size(); n = n + 1) {
			
			// we only show this cite in case we do not render a local summary or in case we are rendering a local
			// summary when there is at least one match ("1") in the cite scope string
			if(local.equals("false") || (local.equals("true") && citeScopes.get(n).contains("1"))) {
	
				// add anchor
				outs.append("<ac:structured-macro ac:name=\"anchor\"><ac:parameter ac:name=\"\">");
				outs.append("CiteSummary_");
				outs.append(n + 1);
				outs.append("</ac:parameter></ac:structured-macro>");
	
				// new list entry
				outs.append("<li VALUE=\"");
				outs.append(Integer.toString(n + 1));
				outs.append("\">");
				
				// visual output for the links to the cites
				if((local.equals("false") && (citeScopes.get(n).length() > 1)) ||
					(local.equalsIgnoreCase("true") && (StringUtils.countMatches(citeScopes.get(n), "1") > 1))) {
					outs.append("↑ ");
				}
				for(int m = 0; m < citeScopes.get(n).length(); m = m + 1) {
					
					// is this cite recurrence in our rendering scope?
					// full scope if not local
					if(local.equals("false") || (local.equals("true") && (citeScopes.get(n).substring(m, m + 1)).equals("1"))) {
						outs.append("<ac:link ac:anchor=\"");
						outs.append("SingleCite_");
						outs.append(Integer.toString(n + 1));
						outs.append("_");
						outs.append(Integer.toString(m + 1));
						outs.append("\"><ac:link-body>");
						if((local.equals("false") && (citeScopes.get(n).length() > 1)) ||
							(local.equalsIgnoreCase("true") && (StringUtils.countMatches(citeScopes.get(n), "1") > 1))) {
							outs.append("<sup>");
							outs.append(m + 1);
							outs.append(" </sup>");
						}
						else {
							outs.append("↑");
						}
						
						outs.append(" </ac:link-body></ac:link>");
					}
				}
				
				// add the cite and end of list entry
				outs.append(cites.get(n));
				outs.append("</li>");
			}
		}
		
		// end of list
		outs.append("</ol>");
		
		log.debug(outs.toString());
		
		// convert the output from storage to view
		try {
			return xhtmlUtils.convertStorageToView(outs.toString(), conversionContext);
		}
		catch (XMLStreamException | XhtmlException e) {
        	return e.toString();
        }
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}
