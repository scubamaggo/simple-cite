package de.edrup.confluence.plugins;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jbibtex.BibTeXDatabase;
import org.jbibtex.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Draft;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.sal.api.message.I18nResolver;

import de.undercouch.citeproc.CSL;
import de.undercouch.citeproc.bibtex.BibTeXConverter;
import de.undercouch.citeproc.bibtex.BibTeXItemDataProvider;
import de.undercouch.citeproc.output.Bibliography;

import org.apache.commons.lang3.StringEscapeUtils;

public class CiteManager {
	
	private final XhtmlContent xhtmlUtils;
	private final AttachmentManager attachmentMan;
	private final PermissionManager permissionMan;
	private final PageManager pageMan;
	private ArrayList<String> citeIDs= new ArrayList<String>();
	private ArrayList<String> cites = new ArrayList<String>();
	private ArrayList<String> citeScopes = new ArrayList<String>();
	private final Cache<String,String> cache;
	private final I18nResolver i18n;
	
	private static final Logger log = LoggerFactory.getLogger(CiteManager.class);
	

	// Constructor
	public CiteManager(XhtmlContent xhtmlUtils, AttachmentManager attachmentMan,
			PermissionManager permissionMan, PageManager pageMan,
			CacheManager cacheMan, I18nResolver i18n) {
		this.xhtmlUtils = xhtmlUtils;
		this.attachmentMan = attachmentMan;
		this.permissionMan = permissionMan;
		this.pageMan = pageMan;
		this.i18n = i18n;
		cache = cacheMan.getCache(CiteManager.class.getName() + ".cache", 
			null, new CacheSettingsBuilder().expireAfterAccess(56, TimeUnit.DAYS).build());
		clear();
	}
	
	
	// scan the given content for all single-site macros and build a list of:
	// citeIDs, cites and counts of each cite
	@SuppressWarnings("unchecked")
	public void prepareForSummary(String content, String bibTEXFormat, ConversionContext conversionContext, ContentEntityObject ceo) throws MacroExecutionException {
		
		// clear the cites
		clear();
		
		// init the counter of cite summaries
		int citeSummaryCount = 0;
		
		// get the last instance number of a cite summary on this page and add one to it => this is our cite summary
		Integer mySummary = conversionContext.hasProperty("de.edrup.confluence.plugins.simple-cite.lastSummary") ?
			(Integer) conversionContext.getProperty("de.edrup.confluence.plugins.simple-cite.lastSummary") + 1 : 1;
		conversionContext.setProperty("de.edrup.confluence.plugins.simple-cite.lastSummary", mySummary);
		
		ArrayList <String> temp_citeIds = new ArrayList<String>();
		ArrayList <Integer> temp_citeCounts = new ArrayList<Integer>();
		
		// get the list of cites and recurrences from the conversion context 
		if(conversionContext.hasProperty("de.edrup.confluence.plugins.simple-cite.citeIDs")) {
			temp_citeIds.addAll((ArrayList <String>) conversionContext.getProperty("de.edrup.confluence.plugins.simple-cite.citeIDs"));
		}
		if(conversionContext.hasProperty("de.edrup.confluence.plugins.simple-cite.citeCounts")) {
			temp_citeCounts.addAll((ArrayList <Integer>) conversionContext.getProperty("de.edrup.confluence.plugins.simple-cite.citeCounts"));
		}
		
		
		// generate a list of MacroDefinitions from the given content
		final List<MacroDefinition> macros = getMacroDefs(content, conversionContext);
		
		// loop through all macros we found and check whether they are a simple-cite(-short) macro
		for(MacroDefinition mdf : macros) {
			
			if(mdf.getName().equals("single-cite-short") || mdf.getName().equals("single-cite") ||
					mdf.getName().equals("bibtex-cite") || mdf.getName().equals("bibtex-cite-link") ||
					mdf.getName().equals("single-cite-import")) {
					
				String citeID = mdf.getParameter("citeID");
				String defineOnly = (mdf.getParameters().containsKey("defineOnly")) ?  mdf.getParameter("defineOnly") : "false";
				
				// store citeID and cite only if it is not yet in the list
				if(!citeIDs.contains(citeID)) {
					
					// add the cite ID to the list
					citeIDs.add(citeID);
					
					// init the cite scope string
					citeScopes.add("");
					
					// in case we are handling a single-cite or a bibtex-cite-link
					if(mdf.getName().equals("single-cite") || mdf.getName().equals("bibtex-cite-link")) {
						
						String body = getBody(mdf);
						
						// just add the body in case of a single cite
						if(mdf.getName().equals("single-cite")) {
							cites.add(body);
						}
						
						// add the BibTeX cite inserted into the body in case of bibtext-cite-link
						if(mdf.getName().equals("bibtex-cite-link")) {
							
							// replace the defined string by the BibTeX cite
							if(body.contains("$BibTeX")) {
								body = body.replace("$BibTeX", handleBibTEXCite(mdf.getParameters(), bibTEXFormat, ceo));
							}
							else {
								body = i18n.getText("de.edrup.confluence.plugins.simple-cite.message.keyword.missing");
							}
							
							// add the BibTeX cite followed by the body
							cites.add(body);
						}
					}
					
					// in case we are handling a short-cite we are facing a use error
					if(mdf.getName().equals("single-cite-short")) {
						cites.add(i18n.getText("de.edrup.confluence.plugins.simple-cite.message.short.cite.too.early"));
					}
					
					// in case we are handling BibTeX cite add the cite defined in the BibTEX file
					if(mdf.getName().equals("bibtex-cite")) {
						cites.add(handleBibTEXCite(mdf.getParameters(), bibTEXFormat, ceo));
					}
					
					// in case we have to handle an imported cite
					if(mdf.getName().equals("single-cite-import")) {
						cites.add(importCite(mdf.getParameters(), ceo));
					}
				}
				
				if(defineOnly.equals("false")) {
					// append "1" in case the cite is in the scope of the local cite summary (defined prior to the current cite summary after the last one)
					// or "0" otherwise
					String inScope = (citeSummaryCount == (mySummary -1)) ? "1" : "0";
					citeScopes.set(citeIDs.indexOf(citeID), (String) (citeScopes.get(citeIDs.indexOf(citeID))).concat(inScope));
				
					// in case we reach a cite summary increment the count
					if(mdf.getName().equals("cite-summary")) {
						citeSummaryCount += 1;
					}
				}
					
			}				
		}
	}
	
	
	// register a cite and return the number of the cite (starting from 0)
	@SuppressWarnings("unchecked")
	public int registerCiteID(ConversionContext cc, String citeID, Boolean hidePlaceholder) {
		ArrayList <String> temp_citeIDs = new ArrayList<String>();
		ArrayList <Integer> temp_citeCounts = new ArrayList<Integer>();
		
		// get the list of cites and recurrences from the conversion context 
		if(cc.hasProperty("de.edrup.confluence.plugins.simple-cite.citeIDs")) {
			temp_citeIDs.addAll((ArrayList <String>) cc.getProperty("de.edrup.confluence.plugins.simple-cite.citeIDs"));
		}
		if(cc.hasProperty("de.edrup.confluence.plugins.simple-cite.citeCounts")) {
			temp_citeCounts.addAll((ArrayList <Integer>) cc.getProperty("de.edrup.confluence.plugins.simple-cite.citeCounts"));
		}
		
		// in case the cite is not yet in our list
		if(!temp_citeIDs.contains(citeID)) {
			// add the citeID to the list
			temp_citeIDs.add(citeID);
			// the initial counter would be one
			if(!hidePlaceholder)
				temp_citeCounts.add(1);
			else 
				temp_citeCounts.add(0);
		}
		
		// in case the cite is already in our list
		else {
			// we just increase the recurrences
			temp_citeCounts.set(temp_citeIDs.indexOf(citeID), temp_citeCounts.get(temp_citeIDs.indexOf(citeID)) + 1);
		}
		
		// store the values back in the conversion context for the next macro
		cc.setProperty("de.edrup.confluence.plugins.simple-cite.citeIDs", temp_citeIDs);
		cc.setProperty("de.edrup.confluence.plugins.simple-cite.citeCounts", temp_citeCounts);
		
		// return the number of the cite (starting from 0)
		return temp_citeIDs.indexOf(citeID);
	}
	
	
	// get the current recurrence count of the given citeID
	@SuppressWarnings("unchecked")
	public int getCiteIDCount(ConversionContext cc, String citeID) {
		ArrayList <String> temp_citeIDs = new ArrayList<String>();
		ArrayList <Integer> temp_citeCounts = new ArrayList<Integer>();
		
		// get the list of cites and recurrences from the conversion context 
		if(cc.hasProperty("de.edrup.confluence.plugins.simple-cite.citeIDs")) {
			temp_citeIDs.addAll((ArrayList <String>) cc.getProperty("de.edrup.confluence.plugins.simple-cite.citeIDs"));
		}
		if(cc.hasProperty("de.edrup.confluence.plugins.simple-cite.citeCounts")) {
			temp_citeCounts.addAll((ArrayList <Integer>) cc.getProperty("de.edrup.confluence.plugins.simple-cite.citeCounts"));
		}
		
		// return the count stored at the position of the given citeID
		return temp_citeCounts.get(temp_citeIDs.indexOf(citeID));
	}
	
	
	// get the list of cite IDs
	public ArrayList<String> getCiteIDs() {
		return citeIDs;
	}
	
	
	// get the list of cites
	public ArrayList<String> getCites() {
		return cites;
	}
	
	
	// return the cite scope
	// the string contains of a string of 1 and 0
	// 1 means that the cite is in the local scope of the cite summary, 0 means it is not
	public ArrayList<String> getCiteScopes() {
		return citeScopes;
	}
	

	// clear all lists
	private void clear() {
		citeIDs.clear();
		cites.clear();
		citeScopes.clear();
	}
	
	
	// get all MacroDefinitions of the provided content
	private List<MacroDefinition> getMacroDefs(String content, ConversionContext conversionContext) throws MacroExecutionException {
		// generate a list of MacroDefinitions from the given content
		final List<MacroDefinition> macros = new ArrayList<MacroDefinition>();
		try
		{
			xhtmlUtils.handleMacroDefinitions(content, conversionContext, new MacroDefinitionHandler()
			{
				@Override
				public void handle(MacroDefinition macroDefinition) {
					macros.add(macroDefinition);
				}
			});
		}
        catch (XhtmlException e) {
        	throw new MacroExecutionException(e);
        }
		
		return macros;
	}
	
	
	// handle a BibTeX cite
	private String handleBibTEXCite(Map<String, String> bibTEXParams, String bibTEXFormat, ContentEntityObject pageCEO) {

		Attachment bibTEXFile = null;
			
		// get the right content entity object
		ContentEntityObject ceo = findAttachmentsPageCEO(bibTEXParams, pageCEO);
				
		if(ceo != null) {
		
			// check for access rights
			ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
			if(permissionMan.hasPermission(confluenceUser, Permission.VIEW, ceo)) {
				// get the attachment and the format
				if(bibTEXParams.containsKey("name")) {
					bibTEXFile = attachmentMan.getAttachment(ceo, bibTEXParams.get("name"));
				}
			}
			else {
				return i18n.getText("de.edrup.confluence.plugins.simple-cite.message.improper.access.rights");
			}
		}
		else {
			return i18n.getText("de.edrup.confluence.plugins.simple-cite.message.page.not.found");
		}
		
		// do we have a proper BibTeX file?
		if(bibTEXFile != null) {
			
			// generate the cache string (we are looking for that string in the cache)
			// we are using the cache to increase performance
			String cacheString = bibTEXFile.getIdAsString() + ":" +
					Integer.toString(bibTEXFile.getVersion()) + ":" + 
					bibTEXParams.get("citeID") + ":" +
					bibTEXFormat + "160";
			
			// in case we have no hit in cache
			if(cache.get(cacheString) == null) {
				
				// get the cite
				String bibTEXCite = getBibTEXCite(bibTEXFile, bibTEXParams.get("citeID"), bibTEXFormat);
				
				// put it into the cache
				cache.put(cacheString, bibTEXCite);
				
				// and return it
				return bibTEXCite;
			}
			// in case we have a hit in the cache
			else {
				// return the cache
				return cache.get(cacheString);
			}
		}
		else {
			return i18n.getText("de.edrup.confluence.plugins.simple-cite.message.access.problem");
		}
	}
	
	
	// return the BibTeX cite with the ID citeID in the bib-file attachment in the specified format
	private String getBibTEXCite(Attachment attachment, String citeID, String format) {
		try {
			// generate a citation string for the citeID out of the BibTEX file
			// the format is the CSL format
			// the following code follows the examples from
			// http://michel-kraemer.github.io/citeproc-java/
			BibTeXDatabase db = new BibTeXConverter().loadDatabase(attachmentMan.getAttachmentData(attachment));
			BibTeXItemDataProvider provider = new BibTeXItemDataProvider();
			provider.addDatabase(db);
			CSL citeproc = new CSL(provider, "/csl/" + format + ".csl");
			citeproc.setOutputFormat("html");
			citeproc.setConvertLinks(true);
			citeproc.makeCitation(citeID);
			Bibliography b = citeproc.makeBibliography();
			String bibTEXCite = b.getEntries()[0];
			bibTEXCite = bibTEXCite.replaceAll("[<](/)?div[^>]*[>]", "").replace("<i>", "<em>").replace("</i>", "</em>");
			
			return bibTEXCite;
		}
		
		// in case of an exception
		catch(IOException | ParseException | IllegalArgumentException | IndexOutOfBoundsException e) {
			// add log entry
			log.error(e.getMessage());
			
			// return an error message including the exception message
			return i18n.getText("de.edrup.confluence.plugins.simple-cite.message.bibtex.error") + e.getMessage();
		}
	}
	
	
	// return all the cites in a BibTeX file as a table
	public String getAllBibTeXCites(Map<String, String> bibTEXParams, ContentEntityObject pageCEO) {
		
		Attachment bibTEXFile = null;
		
		// get the right content entity object
		ContentEntityObject ceo = findAttachmentsPageCEO(bibTEXParams, pageCEO);
				
		if(ceo != null) {
		
			// check for access rights
			ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
			if(permissionMan.hasPermission(confluenceUser, Permission.VIEW, ceo)) {
				// get the attachment and the format
				if(bibTEXParams.containsKey("name")) {
					bibTEXFile = attachmentMan.getAttachment(ceo, bibTEXParams.get("name"));
				}
			}
			else {
				return i18n.getText("de.edrup.confluence.plugins.simple-cite.message.improper.access.rights");
			}
		}
		else {
			return i18n.getText("de.edrup.confluence.plugins.simple-cite.message.page.not.found");
		}
		
		// do we have a proper BibTeX file?
		if(bibTEXFile != null) {
			try {
				
				String format = bibTEXParams.containsKey("format") ? bibTEXParams.get("format") : "ieee";
				
				BibTeXDatabase db = new BibTeXConverter().loadDatabase(attachmentMan.getAttachmentData(bibTEXFile));
				BibTeXItemDataProvider provider = new BibTeXItemDataProvider();
				provider.addDatabase(db);
				CSL citeproc = new CSL(provider, "/csl/" + format + ".csl");
				citeproc.setOutputFormat("text");
				provider.registerCitationItems(citeproc);
				Bibliography b = citeproc.makeBibliography();
				
				String allCitesInTable = i18n.getText("de.edrup.confluence.plugins.simple-cite.bibtex-listing.table");
				String cites[] = b.getEntries();
				String citeIDs[] = b.getEntryIds();
				
				for(int n = 0; n < cites.length; n++) {
					String cite = cites[n];
					
					// cut the cite number out of string - it can be [1] or 1.
					if(cite.startsWith("[")) {
						cite = cite.replaceFirst("\\[.*\\] *", "");
					}
					if(cite.matches("^\\d+\\.")) {
						cite = cite.replaceFirst("^\\d+\\.", "");
					}
					
					// escape special characters
					cite = StringEscapeUtils.escapeHtml4(cite);
					
					allCitesInTable = allCitesInTable + "<tr><td>"+ citeIDs[n] + "</td><td>" + cite + "</td></tr>";
				}
				
				allCitesInTable = allCitesInTable + "</tbody></table>";
					
				return allCitesInTable;
			}
			
			// in case of an exception
			catch(Exception e) {
				// add log entry
				log.error(e.getMessage());
				
				// return an error message including the exception message
				return "BibTeX error: " + e.getMessage();
			}
			
		}
		else {
			return i18n.getText("de.edrup.confluence.plugins.simple-cite.message.access.problem");
		}
	}
	
	
	// find the right ContentEntityObject
	private ContentEntityObject findAttachmentsPageCEO(Map<String, String> parameters, ContentEntityObject pageCEO) {
		
		// do some init stuff
		ContentEntityObject ceo = null;
		Page page = null;
		
		// find the right page
		// in case the parameter page is not given we take the current entity 
		if(!parameters.containsKey("page")) {
			ceo = pageCEO;
		}
		// in case the parameter page is given
		else {
			// get the parameter page
			String pageParam = parameters.get("page");
			String spaceParam = "";
			
			// is the "unofficial" parameter "space" given?
			// remark: the js provided by Confluence only sets it in case the space is different from the current space
			if(parameters.containsKey("space")) {
				spaceParam = parameters.get("space");
			}
			// in case "space" is not in the list of parameters
			else {
				// we will find the page/draft in our space
				if(pageCEO instanceof Page) {
					spaceParam = ((Page) pageCEO).getSpaceKey();
				}
				if(pageCEO instanceof Draft) {
					spaceParam = ((Draft) pageCEO).getDraftSpaceKey();
				}
			}
			
			// get the page
			page = pageMan.getPage(spaceParam, pageParam);
		
			// in case we found a page get the entity of it
			if(page != null) {
				ceo = page.getEntity();
			}
		}
		
		// return the content entity object we found
		return ceo;
	}
	
	
	// get the cite for the given citeID from another page
	private String importCite(Map<String, String> parameters, ContentEntityObject pageCEO) {
		
		// determine the page we want to import from
		Page p = null;
		if(parameters.get("page").contains(":")) {
			String[] spaceAndPage = parameters.get("page").split(":");
			p = pageMan.getPage(spaceAndPage[0], spaceAndPage[1]);
		}
		else {
			p = pageMan.getPage(((Page) pageCEO).getSpaceKey(), parameters.get("page"));
		}
		
		if(p == null) {
			return i18n.getText("de.edrup.confluence.plugins.simple-cite.message.import.page.notfound");
		}
		
		// try to find the corresponding macro
		try {
			final List<MacroDefinition> macros = getMacroDefs(p.getBodyAsString(), new DefaultConversionContext(p.toPageContext()));
			
			// loop through all macros we found and whether they match our citeID
			for(MacroDefinition mdf : macros) {
				if(mdf.getName().equals("single-cite") && mdf.getParameters().containsKey("citeID") && mdf.getParameter("citeID").equals(parameters.get("citeID"))) {
					return getBody(mdf);
				}
			}
		
			// not found
			return i18n.getText("de.edrup.confluence.plugins.simple-cite.message.import.cite.notfound");
		}
		catch(Exception e) {
			return e.toString();
		}
	}
	
	
	// get the body from the given macro
	private String getBody(MacroDefinition mdf) {
		// get the body
		// this has to be so complex as mdf.getBodyText cuts out macros in the body
		// issue #7
		StringWriter body_sw = new StringWriter();
		try {
			mdf.getBody().getStorageBodyStream().writeTo(body_sw);
		}
		catch(IOException | NoSuchMethodError e) {
			// not all supported versions of Confluence support the above function
			body_sw.write(mdf.getBodyText());
		}
		
		// convert StringWriter to string and cut paragraph out of body
		String body = body_sw.toString();
		if(body.startsWith("<p>") && body.endsWith("</p>")) {
			body = body.replaceFirst("<p>", "").replaceFirst("</p>", "");
		}
		
		return body;
	}
}
