package de.edrup.confluence.plugins;

import com.atlassian.confluence.xhtml.api.XhtmlContent;

public class CiteImport extends Cite {

	public CiteImport(XhtmlContent xhtmlUtils, CiteManager citeMan) {
		super(xhtmlUtils, citeMan);
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}
}
