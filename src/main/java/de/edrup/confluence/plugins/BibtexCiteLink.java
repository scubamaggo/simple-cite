package de.edrup.confluence.plugins;

import com.atlassian.confluence.xhtml.api.XhtmlContent;

public class BibtexCiteLink extends Cite {

	public BibtexCiteLink(XhtmlContent xhtmlUtils, CiteManager citeMan) {
		super(xhtmlUtils, citeMan);
	}
}
