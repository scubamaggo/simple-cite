package de.edrup.confluence.plugins;

import com.atlassian.confluence.xhtml.api.XhtmlContent;

public class BibtexCite extends Cite {

	public BibtexCite(XhtmlContent xhtmlUtils, CiteManager citeMan) {
		super(xhtmlUtils, citeMan);
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}
}
