package de.edrup.confluence.plugins;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.xhtml.api.XhtmlContent;

public class BibtexListing implements Macro {
	
	private final CiteManager citeMan;
	private final XhtmlContent xhtmlUtils;
	
	private static final Logger log = LoggerFactory.getLogger(BibtexListing.class);

	public BibtexListing(CiteManager citeMan, XhtmlContent xhtmlUtils) {
		this.citeMan = citeMan;
		this.xhtmlUtils = xhtmlUtils;
	}

	@Override
	public String execute(Map<String, String> parameters, String body, ConversionContext context) {
		
		String allBibTeXCites = citeMan.getAllBibTeXCites(parameters, context.getEntity());
		log.debug(allBibTeXCites);
		
		try {
			return xhtmlUtils.convertStorageToView(allBibTeXCites, context);
		}
		catch (Exception e) {
        	return e.toString();
		}
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}
