package de.edrup.confluence.plugins;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.confluence.security.Permission;


public class SpaceCiteSummary implements Macro {
	
	private final SpaceManager spaceManager;
	private final PageManager pageManager;
	private final XhtmlContent xhtmlUtils;
	private final SettingsManager settingsManager;
	private final PermissionManager permissionManager;
	private final CiteManager citeMan;
	
	private static final Logger log = LoggerFactory.getLogger(SpaceCiteSummary.class);

	public SpaceCiteSummary(SpaceManager spaceManager, PageManager 
			pageManager, XhtmlContent xhtmlUtils, SettingsManager settingsManager,
			PermissionManager permissionManager, CiteManager citeMan) {
		this.spaceManager = spaceManager;
		this.pageManager = pageManager;
		this.xhtmlUtils = xhtmlUtils;
		this.settingsManager = settingsManager;
		this.permissionManager = permissionManager;
		this.citeMan = citeMan;
	}

	@Override
	public String execute(Map<String, String> parameters, String bodyContent,
			ConversionContext conversionContext) throws MacroExecutionException {
		
		// the current user
		ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
		
		// get parameters
		String space_list = (parameters.containsKey("space_list")) ? parameters.get("space_list") : "";
		String bibTEXFormat = parameters.containsKey("format") ? parameters.get("format") : "ieee";

		// split list
		ArrayList<String> spacesToCheck = new ArrayList<String>();
		if(space_list.length() > 0) {
			spacesToCheck.addAll(Arrays.asList(space_list.split("\\s*,\\s*")));
		}
		
		// in case the list is empty we add the current space
		if(spacesToCheck.size() == 0) {
			spacesToCheck.add(conversionContext.getSpaceKey());
		}
		
		ArrayList<String> citeIDs = new ArrayList<String>();
		ArrayList<String> cites = new ArrayList<String>();
		ArrayList<String> pages = new ArrayList<String>();
		
		// loop through all spaces in the list
		for(String spaceName : spacesToCheck) {
			
			Space sp = spaceManager.getSpace(spaceName);
			
			if(sp != null) {
			
				// loop through all pages in the space
				for(Page page : pageManager.getPages(sp, true)) {
					
					// is the current user allowed to view the page?
					if(permissionManager.hasPermission(confluenceUser, Permission.VIEW, page)) {
						
						// we only let the searcher run in case the body contains the expression single-cite
						if(page.getBodyAsString().contains("single-cite") || page.getBodyAsString().contains("bibtex-cite")) {
							
							citeMan.prepareForSummary(page.getBodyAsString(), bibTEXFormat, conversionContext, page.getEntity());
							
							// for all single cite macros we found on the page
							for(int i = 0; i < citeMan.getCiteIDs().size(); i = i + 1) {
								
								// get the citeID
								String citeID = citeMan.getCiteIDs().get(i);
								
								// in case we already have that citeID in our space wide list
								if(citeIDs.contains(citeID)) {
									int noOfCiteID = citeIDs.indexOf(citeID);
									// add the page to the space wide list
									pages.set(noOfCiteID, pages.get(noOfCiteID) + "<p>" + getTitleAsLink(page) + "</p>");
									// add the cite to the space wide list only if we don't have it (the same text) already
									if(!cites.get(noOfCiteID).contains(citeMan.getCites().get(i))) {
										cites.set(noOfCiteID, cites.get(noOfCiteID) + "<p>" + citeMan.getCites().get(i) + "</p>");
									}
								}
								
								// if the citeID is not yet included in our space wide list
								else {
									// add the cite ID to the space wide list
									citeIDs.add(citeID);
									// add the page to the space wide list
									pages.add("<p>" + getTitleAsLink(page) + "</p>");
									// add the cite to the space wide list
									cites.add("<p>" + citeMan.getCites().get(i) + "</p>");
								}
							}
						}
					}
				}
			}
		}
		
		// generate visual output
		StringBuilder builder = new StringBuilder();
		builder.append("<table><tbody><tr><th>cite ID</th><th>cites belonging to the cite ID</th><th>pages containing the cite ID</th></tr>");
		for(int i = 0; i < citeIDs.size(); i = i + 1) {
			builder.append("<tr><td>");
			builder.append(citeIDs.get(i));
			builder.append("</td><td>");
			builder.append(cites.get(i));
			builder.append("</td><td>");
			builder.append(pages.get(i));
			builder.append("</td></tr>");
		}
		builder.append("</tbody></table>");
		
		log.debug(builder.toString());
		
		try {
			return xhtmlUtils.convertStorageToView(builder.toString(), conversionContext);
		}
		catch (Exception e) {
			return e.toString();
        }
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
	
	// return a link to the page with the link text = page title
	private String getTitleAsLink(Page p) {
		StringBuilder link = new StringBuilder();
		
		link.append("<a href=\"");
		link.append(settingsManager.getGlobalSettings().getBaseUrl());
		link.append(p.getUrlPath());
		link.append("\">");
		link.append(StringEscapeUtils.escapeHtml4(p.getTitle()));
		link.append("</a>");
		
		return link.toString();
	}
}
