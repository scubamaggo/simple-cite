package de.edrup.confluence.plugins;

import java.util.Map;

import javax.xml.stream.XMLStreamException;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.xhtml.api.XhtmlContent;

public class Cite implements Macro {
	
	private final XhtmlContent xhtmlUtils;
	private final CiteManager citeMan;

	public Cite(XhtmlContent xhtmlUtils, CiteManager citeMan) {
		this.xhtmlUtils = xhtmlUtils;
		this.citeMan = citeMan;
	}

	@Override
	public String execute(Map<String, String> parameters, String bodyContent,
			ConversionContext conversionContext) throws MacroExecutionException {
		
		// extract the parameters we need
		// remark: superscript is not always in the parameter map... CONF-23936
		String citeID = parameters.get("citeID");
		String superscript = (parameters.containsKey("superscript")) ? parameters.get("superscript") : "true";
		String citepages = (parameters.containsKey("pages")) ? parameters.get("pages") : "";
		String defineOnly = (parameters.containsKey("defineOnly")) ? parameters.get("defineOnly") : "false";
		
		// cite number and recurrence of the cite (for the anchor)
		int citeNo = citeMan.registerCiteID(conversionContext, citeID, Boolean.parseBoolean(defineOnly));
		int citeCount = citeMan.getCiteIDCount(conversionContext, citeID);
				
		// generate visual output
		StringBuilder outs = new StringBuilder();
		
		// add anchor
		outs.append("<ac:structured-macro ac:name=\"anchor\"><ac:parameter ac:name=\"\">");
		outs.append("SingleCite_");
		outs.append(Integer.toString(citeNo + 1));
		outs.append("_");
		outs.append(Integer.toString(citeCount));
		outs.append("</ac:parameter></ac:structured-macro>");
			
		if(defineOnly.equals("false")) {
			// the number in brackets with link to cite summary
			outs.append("<ac:link ac:anchor=\"");
			outs.append("CiteSummary_");
			outs.append(Integer.toString(citeNo + 1));
			outs.append("\"><ac:link-body>");
			if(superscript.equals("true")) {
				outs.append("<sup>");
			}
			outs.append("[");
			outs.append(Integer.toString(citeNo + 1));
			if(citepages.length() > 0) {
				outs.append(", p.");
				outs.append(citepages);
			}
			outs.append("]");
			if(superscript.equals("true")) {
				outs.append("</sup>");
			}
			outs.append("</ac:link-body></ac:link>");
		}		
		// convert the output from storage to view
		String converted = "";
		try {
			converted = xhtmlUtils.convertStorageToView(outs.toString(), conversionContext);
		}
		catch (XMLStreamException | XhtmlException e) {
        	return e.toString();
        }
		
		// Confluence 5.9.1 inline rendering bug work around
		// start
		converted = "<span class=\"SimpleCite\">" + converted + "</span>";
		// end
		
		return converted;
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.RICH_TEXT;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.INLINE;
	}

}
